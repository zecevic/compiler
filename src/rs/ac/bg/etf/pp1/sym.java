
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sat Jan 21 13:29:29 CET 2017
//----------------------------------------------------

package rs.ac.bg.etf.pp1;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int DIV_EQ = 39;
  public static final int REL_EQUAL = 27;
  public static final int CONST = 10;
  public static final int LPAREN = 46;
  public static final int SEMI = 44;
  public static final int GREATER = 29;
  public static final int CONTINUE = 18;
  public static final int CHARVAL = 4;
  public static final int LESS = 31;
  public static final int FOR = 16;
  public static final int MINUS = 23;
  public static final int RPAREN = 47;
  public static final int STATIC = 19;
  public static final int AND = 33;
  public static final int MUL_EQ = 38;
  public static final int OR = 34;
  public static final int COMMA = 45;
  public static final int CLASS = 7;
  public static final int INC = 42;
  public static final int DIV = 25;
  public static final int PLUS = 22;
  public static final int IF = 11;
  public static final int DOT = 52;
  public static final int FACT = 41;
  public static final int EOF = 0;
  public static final int RETURN = 14;
  public static final int EQUAL = 35;
  public static final int NEW = 12;
  public static final int error = 1;
  public static final int PROGRAM = 5;
  public static final int MUL = 24;
  public static final int STRVAL = 21;
  public static final int NUMBER = 2;
  public static final int MOD = 26;
  public static final int BREAK = 6;
  public static final int IDENT = 3;
  public static final int VOID = 15;
  public static final int MOD_EQ = 40;
  public static final int LSQBRA = 48;
  public static final int LBRACE = 50;
  public static final int ELSE = 9;
  public static final int READ = 13;
  public static final int RSQBRA = 49;
  public static final int LESS_EQ = 32;
  public static final int RBRACE = 51;
  public static final int BOOLCONST = 20;
  public static final int EXTENDS = 17;
  public static final int PLUS_EQ = 36;
  public static final int DIFF = 28;
  public static final int MINUS_EQ = 37;
  public static final int DEC = 43;
  public static final int PRINT = 8;
  public static final int GREATER_EQ = 30;
}

