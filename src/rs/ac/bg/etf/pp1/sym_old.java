package rs.ac.bg.etf.pp1;

public class sym_old {

	//Keywords
	public static final int PROGRAM = 1; 
	public static final int BREAK = 2;
	public static final int CLASS = 3; 
	public static final int ELSE = 4; 
	public static final int CONST = 5; 
	public static final int IF = 6; 
	public static final int NEW = 7;
	public static final int PRINT = 8;
	public static final int READ  = 9; 
	public static final int RETURN = 10; 
	public static final int VOID = 11; 
	public static final int FOR = 12; 
	public static final int EXTENDS = 13; 
	public static final int CONTINUE = 14; 
	public static final int STATIC = 15; 
	
	
	//Identifiers
	public static final int IDENT = 16; 
	
	//Constants 
	public static final int NUMBER = 17; 
	public static final int BOOLCONST = 18; 
	//char and string
	public static final int CHARVAL = 19; 
	public static final int STRVAL = 51; 
	//Operators
	public static final int PLUS = 20; 
	public static final int MINUS = 21; 
	public static final int MUL = 22; 
	public static final int DIV = 23; 
	public static final int MOD = 24; 
	public static final int REL_EQUAL = 25;  // ==
	public static final int DIFF = 26; // !=
	public static final int GREATER = 27; // >
	public static final int GREATER_EQ = 28; 
	public static final int LESS = 29; 
	public static final int LESS_EQ = 30; 
	public static final int AND = 31; 
	public static final int OR = 32; 
	public static final int EQUAL = 33; 
	public static final int PLUS_EQ = 34; //+=
	public static final int MINUS_EQ = 35; 
	public static final int MUL_EQ = 36; 
	public static final int DIV_EQ = 37; 
	public static final int MOD_EQ = 38; 
	public static final int INC = 39; //++
	public static final int DEC = 40; //--
	public static final int SEMI = 41; 
	public static final int COMMA = 42; 
	public static final int DOT = 43; 
	public static final int LPAREN = 44;
	public static final int RPAREN = 45;
	public static final int LSQBRA = 46; 
	public static final int RSQBRA = 47; 
	public static final int LBRACE = 48; 
	public static final int RBRACE = 49; 
	
	public static final int EOF = 50; 

	
	
	
}
