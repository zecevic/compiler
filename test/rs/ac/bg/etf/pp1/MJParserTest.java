package rs.ac.bg.etf.pp1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import java_cup.runtime.Symbol;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rs.ac.bg.etf.pp1.util.Log4JUtils;
import rs.etf.pp1.mj.runtime.Code;


public class MJParserTest {

	static {
		DOMConfigurator.configure(Log4JUtils.instance().findLoggerConfigFile());
		Log4JUtils.instance().prepareLogFile(Logger.getRootLogger());
	}
	
	public static void main(String[] args) throws Exception {
		
		Logger log = Logger.getLogger(MJParserTest.class);
		
		Reader br = null;
		try {
			
			if (args.length < 1) {
				log.error("Not enough arguments supplied! Usage: MJTest <source-file>");
				return;
			}
		//	File sourceCode = new File(args[0]);
			File sourceCode  = new File("test/program.mj"); 
			log.info("Compiling source file: " + sourceCode.getAbsolutePath());
			
			br = new BufferedReader(new FileReader(sourceCode));
			Yylex lexer = new Yylex(br);
			
			MJParser p = new MJParser(lexer);
	        Symbol s = p.parse();  //pocetak parsiranja
	        log.info("======================================"); 
	        log.info("Nivo A"); 
	        log.info(p.globalVarCount +"\t global variables");
	        log.info(p.globalArrayCount+"\t global arrays ");
	        log.info(p.globalConstCount+"\t global constants");
	        log.info(p.localMainVar +"\t local variables in main ");
	        log.info("======================================"); 
	        log.info("Nivo B"); 
	        log.info(p.classStaticMethodCount +"\t static methods in classes");
	        log.info(p.statementBlockCount +"\t statement blocks");
	        log.info(p.methodCallMainCount+"\t method calls in main (with print and read)");
	        log.info(p.formalParsCount +"\t formal parameters ");
	        log.info("======================================"); 
	        log.info("Nivo C"); 
	        log.info(p.classCount +"\t classes");
	        log.info(p.classMethodCount +"\t methods in classes");
	        log.info(p.classVarCount+"\t fields in classes");
	        log.info("======================================"); 
	        
	        UserTab.dump(new UserSymbolTableVisitor());
	        if (!p.errorDetected){
	        	File objFile = new File("test/program.obj"); 
	        	if (objFile.exists())
	        		objFile.delete(); 
	        	Code.write(new FileOutputStream(new File("test/program.obj")));
	        	log.info("Successfully finished parsing");
	        }else {
	        	log.info("Error(s) detected in parsing");
	        }
		} 
		finally {
			if (br != null) try { br.close(); } catch (IOException e1) { log.error(e1.getMessage(), e1); }
		}

	}
	
	
}
