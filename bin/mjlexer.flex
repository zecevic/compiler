package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{
	String buff = new String();
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type) {
		return new Symbol(type, yyline+1, yycolumn);
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol new_symbol(int type, Object value) {
		return new Symbol(type, yyline+1, yycolumn, value);
	}

%}

%cup
%line
%column

%xstate STRING
%xstate COMMENT

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

" " 	{ }
"\b" 	{ }
"\t" 	{ }
"\r\n" 	{ }
"\f" 	{ }

"program"   { return new_symbol(sym.PROGRAM, yytext()); }
"break"		{ return new_symbol(sym.BREAK, yytext()); }
"class"		{ return new_symbol(sym.CLASS, yytext()); }
"else"		{ return new_symbol(sym.ELSE, yytext()); }
"const"		{ return new_symbol(sym.CONST, yytext()); }
"if"		{ return new_symbol(sym.IF, yytext()); }
"new"		{ return new_symbol(sym.NEW, yytext()); }
"print" 	{ return new_symbol(sym.PRINT, yytext()); }
"read"		{ return new_symbol(sym.READ, yytext()); }
"return" 	{ return new_symbol(sym.RETURN, yytext()); }
"void" 		{ return new_symbol(sym.VOID, yytext()); }
"for"		{ return new_symbol(sym.FOR, yytext()); }
"extends"	{ return new_symbol(sym.EXTENDS, yytext()); }
"continue"	{ return new_symbol(sym.CONTINUE, yytext()); }
"static"	{ return new_symbol(sym.STATIC, yytext()); }
"true"		{ return new_symbol(sym.BOOLCONST, 1); }
"false"		{ return new_symbol(sym.BOOLCONST, 0); }

"++"		{ return new_symbol(sym.INC,yytext()); }
"--"		{ return new_symbol(sym.DEC,yytext()); }
"=="		{ return new_symbol(sym.REL_EQUAL,yytext()); }
">="		{ return new_symbol(sym.GREATER_EQ, yytext()); }
"<="		{ return new_symbol(sym.LESS_EQ, yytext()); }
"+="		{ return new_symbol(sym.PLUS_EQ, yytext()); }
"-="		{ return new_symbol(sym.MINUS_EQ, yytext()); }
"*="		{ return new_symbol(sym.MUL_EQ, yytext()); }
"/="		{ return new_symbol(sym.DIV_EQ, yytext()); }
"%="		{ return new_symbol(sym.MOD_EQ, yytext()); }
"+" 		{ return new_symbol(sym.PLUS, yytext()); }
"-"			{ return new_symbol(sym.MINUS, yytext()); }
"*"			{ return new_symbol(sym.MUL, yytext()); }
"/"			{ return new_symbol(sym.DIV, yytext()); }
"%"			{ return new_symbol(sym.MOD, yytext());}
"&&"		{ return new_symbol(sym.AND, yytext()); }
"||"		{ return new_symbol(sym.OR, yytext()); }
"!="		{ return new_symbol(sym.DIFF, yytext()); }
"=" 		{ return new_symbol(sym.EQUAL, yytext()); }
"<"			{ return new_symbol(sym.LESS, yytext());}
">"			{ return new_symbol(sym.GREATER, yytext());}
";" 		{ return new_symbol(sym.SEMI, yytext()); }
"," 		{ return new_symbol(sym.COMMA, yytext()); }
"(" 		{ return new_symbol(sym.LPAREN, yytext()); }
")" 		{ return new_symbol(sym.RPAREN, yytext()); }
"{" 		{ return new_symbol(sym.LBRACE, yytext()); }
"}"			{ return new_symbol(sym.RBRACE, yytext()); }
"["			{ return new_symbol(sym.LSQBRA, yytext()); }
"]"			{ return new_symbol(sym.RSQBRA, yytext()); }
"\."		{ return new_symbol(sym.DOT, yytext()); }

"//" 		     { yybegin(COMMENT); }
<COMMENT> .      { yybegin(COMMENT); }
<COMMENT> "\r\n" { yybegin(YYINITIAL); }
[0-9]+  { return new_symbol(sym.NUMBER, new Integer (yytext())); }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* 	{return new_symbol (sym.IDENT, yytext()); }

"\"" 							{ yybegin(STRING); buff = ""; }
<STRING>"\"" 					{ yybegin(YYINITIAL); return new_symbol(sym.STRVAL, new String(buff)); }
<STRING>[\040-\176|\t|\n|\r\n] 	{ buff += yytext(); }


"'"[\040-\176]"'" { return new_symbol (sym.CHARVAL, new Character(yytext().charAt(1))); }
. { System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1)); }
